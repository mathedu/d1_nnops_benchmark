# d1_nnops_benchmark

#### 介绍
常用向量运算，矩阵运算，卷积运算的在全志D1上的测试

#### 软件架构
软件架构说明


#### 使用说明

0.  请到平头哥开放社区安装工具链及qemu。（csky-qemu-x86_64-Ubuntu-16.04-20210202-1445.tar 和 riscv64-linux-x86_64-20210512.tar) https://occ.t-head.cn/community/download?id=3913221581316624384
1.  请先安装 https://github.com/skywind3000/emake 
2.  emake -b main.mak --ini=emake.ini --print=7
3.  emake -clean main.mak

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
