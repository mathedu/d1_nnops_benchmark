mode: exe

src: NN_operations/vector_operations_assembly.s
src: NN_operations/convolution_operations.c
src: NN_operations/matrix_operations.c
src: NN_operations/pooling_operations.c
src: NN_operations/vector_operations.c
src: Matrix_IO.c
src: NN_algorithms_testbench.c

sflag: -march=rv64gcvxthead
flnk: -static
